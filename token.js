import * as readline from "readline-sync";

import { MatrixAuth } from "matrix-bot-sdk";

const homeserver = "https://matrix.org"
const mxid = readline.question("MXID: ");
const password = readline.question("Passwort: ", { hideEchoBack: true });

const auth = new MatrixAuth(homeserver);
const client = await auth.passwordLogin(mxid, password);
console.log("Copy this access token to your bot's config: ", client.accessToken);
