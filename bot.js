import {
    MatrixClient,
    SimpleFsStorageProvider,
    AutojoinRoomsMixin,
    RustSdkCryptoStorageProvider,
} from "matrix-bot-sdk";

import * as config from "./config.js";

const storageProvider = new SimpleFsStorageProvider("./data/bot.json"); // or any other IStorageProvider
const cryptoProvider = new RustSdkCryptoStorageProvider("./data/crypto");

// Finally, let's create the client and set it to autojoin rooms. Autojoining is typical of bots to ensure
// they can be easily added to any room.
const client = new MatrixClient(config.homeserver, config.accessToken, storageProvider, cryptoProvider);
AutojoinRoomsMixin.setupOnClient(client);

// Before we start the bot, register our command handler
client.on("room.message", handleCommand);

// Now that everything is set up, start the bot. This will start the sync loop and run until killed.
client.start().then(() => console.log("Bot started!"));

// This is the command handler we registered a few lines up
async function handleCommand(roomId, event) {
    // Only handle text messages
    if (event['content']?.['msgtype'] !== 'm.text') return;
    // The bot should not respont to it's own messages
    if (event['sender'] === await client.getUserId()) return;

    await client.sendMessage(roomId, event['content']);
}
